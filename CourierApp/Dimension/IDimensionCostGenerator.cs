﻿namespace CourierApp.Dimension
{
    public interface IDimensionCostGenerator
    {
        DimensionCost GetDimensionCost(double dimension);
    }
}
