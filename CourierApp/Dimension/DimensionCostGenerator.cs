﻿using System.Collections.Generic;
using System.Linq;

namespace CourierApp.Dimension
{
    public class DimensionCostGenerator : IDimensionCostGenerator
    {
        private readonly List<DimensionCost> _dimensionCosts = new List<DimensionCost>();

        public DimensionCostGenerator()
        {
            _dimensionCosts.Add(new DimensionCost
            {
                MaxSize = DimensionCostData.SmallParcel.MaxSize,
                MinSize = DimensionCostData.SmallParcel.MinSize,
                Cost = DimensionCostData.SmallParcel.Cost,
                ParcelType = DimensionCostData.SmallParcel.Type
            });
            _dimensionCosts.Add(new DimensionCost
            {
                MaxSize = DimensionCostData.MediumParcel.MaxSize,
                MinSize = DimensionCostData.MediumParcel.MinSize,
                Cost = DimensionCostData.MediumParcel.Cost,
                ParcelType = DimensionCostData.MediumParcel.Type
            });
            _dimensionCosts.Add(new DimensionCost
            {
                MaxSize = DimensionCostData.LargeParcel.MaxSize,
                MinSize = DimensionCostData.LargeParcel.MinSize,
                Cost = DimensionCostData.LargeParcel.Cost,
                ParcelType = DimensionCostData.LargeParcel.Type
            });
            _dimensionCosts.Add(new DimensionCost
            {
                MaxSize = DimensionCostData.ExtraLargeParcel.MaxSize,
                MinSize = DimensionCostData.ExtraLargeParcel.MinSize,
                Cost = DimensionCostData.ExtraLargeParcel.Cost,
                ParcelType = DimensionCostData.ExtraLargeParcel.Type
            });
        }
        public DimensionCost GetDimensionCost(double dimension)
        {
            return _dimensionCosts.SingleOrDefault(d => dimension >= d.MinSize && dimension < d.MaxSize);
        }
    }
}
