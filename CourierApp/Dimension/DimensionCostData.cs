﻿using CourierApp.Parcel;

namespace CourierApp.Dimension
{
    internal class DimensionCostData
    {
        public class SmallParcel
        {
            public const double MinSize = 0;
            public const double MaxSize = 10;
            public const decimal Cost = 3;
            public const ParcelType Type = ParcelType.Small;
        }

        public class MediumParcel
        {
            public const double MinSize = 10;
            public const double MaxSize = 50;
            public const decimal Cost = 8;
            public const ParcelType Type = ParcelType.Medium;
        }

        public class LargeParcel
        {
            public const double MinSize = 50;
            public const double MaxSize = 100;
            public const decimal Cost = 15;
            public const ParcelType Type = ParcelType.Large;
        }

        public class ExtraLargeParcel
        {
            public const double MinSize = 100;
            public const double MaxSize = double.MaxValue;
            public const decimal Cost = 25;
            public const ParcelType Type = ParcelType.ExtraLarge;
        }
    }
}
