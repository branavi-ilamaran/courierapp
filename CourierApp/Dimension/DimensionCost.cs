﻿using CourierApp.Parcel;

namespace CourierApp.Dimension
{
    public class DimensionCost
    {
        public double MinSize { get; set; }

        public double MaxSize { get; set; }

        public decimal Cost { get; set; }

        public ParcelType ParcelType { get; set; }
    }
}
