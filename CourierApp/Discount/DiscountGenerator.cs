﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourierApp.Parcel;

namespace CourierApp.Discount
{
    public class DiscountGenerator : IDiscountGenerator
    {
        public decimal GetTotalDiscount(List<Parcel.Parcel> parcels)
        {
            return ApplySmallParcelMania(parcels) + ApplyMediumParcelMania(parcels) + ApplyMixedParcelMania(parcels);
        }
        private static decimal ApplySmallParcelMania(IEnumerable<Parcel.Parcel> parcels)
        {
            var parcelList = parcels.Where(c => c.Type == ParcelType.Small).ToList();
            if (parcelList.Count < 4) return 0;
            var fourthParcel = parcelList.ElementAt(3);
            return fourthParcel?.Cost ?? 0;
        }

        private static decimal ApplyMediumParcelMania(IEnumerable<Parcel.Parcel> parcels)
        {
            var parcelList = parcels.Where(c => c.Type == ParcelType.Medium).ToList();
            if (parcelList.Count < 3) return 0;
            var third = parcelList.ElementAt(2);
            return third?.Cost ?? 0;
        }

        private static decimal ApplyMixedParcelMania(List<Parcel.Parcel> parcels)
        {
            if (parcels.Count < 5) return 0;
            var fifth = parcels.ElementAt(4);
            return fifth?.Cost ?? 0;
        }
    }
}
