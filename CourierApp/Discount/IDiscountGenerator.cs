﻿using System.Collections.Generic;

namespace CourierApp.Discount
{
    public interface IDiscountGenerator
    {
        decimal GetTotalDiscount(List<Parcel.Parcel> parcels);
    }
}
