﻿namespace CourierApp.FastTrack
{
    public class FastTrackCostGenerator : IFastTrackCostGenerator
    {
        public decimal CalculateCost(FastTrack fastTrack)
        {
            if (!fastTrack.Enabled)
            {
                return 0;
            }
            return fastTrack.NormalDeliveryCost * 2 + fastTrack.AdditionalCost;
        }
    }
}
