﻿namespace CourierApp.FastTrack
{
    public interface IFastTrackCostGenerator
    {
        decimal CalculateCost(FastTrack fastTrack);
    }
}
