﻿namespace CourierApp.FastTrack
{
    public class FastTrack
    {
        public bool Enabled { get; set; }

        public decimal NormalDeliveryCost { get; set; }

        public decimal AdditionalCost { get; set; }
    }
}
