﻿namespace CourierApp.Parcel
{
    public class Parcel
    {
        public double Dimension { get; set; }

        public decimal Cost { get; set; }

        public double Weight { get; set; }

        public ParcelType Type { get; set; }
    }
}
