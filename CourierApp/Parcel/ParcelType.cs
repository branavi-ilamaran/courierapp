﻿namespace CourierApp.Parcel
{
    public enum ParcelType
    {
        Small,
        Medium,
        Large,
        ExtraLarge,
        Heavy
    }
}
