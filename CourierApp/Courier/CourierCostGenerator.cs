﻿using System.Linq;
using CourierApp.Dimension;
using CourierApp.Discount;
using CourierApp.FastTrack;
using CourierApp.Parcel;
using CourierApp.Weight;

namespace CourierApp.Courier
{
    public class CourierCostGenerator : ICourierCostGenerator
    {
        private readonly IDimensionCostGenerator _dimensionCostGenerator;
        private readonly IFastTrackCostGenerator _fastTrackCostGenerator;
        private readonly IWeightGenerator _weightGenerator;
        private readonly IDiscountGenerator _discountGenerator;

        public CourierCostGenerator(IDimensionCostGenerator dimensionCostGenerator,
            IFastTrackCostGenerator fastTrackCostGenerator, IWeightGenerator weightGenerator, IDiscountGenerator discountGenerator)
        {
            _fastTrackCostGenerator = fastTrackCostGenerator;
            _dimensionCostGenerator = dimensionCostGenerator;
            _weightGenerator = weightGenerator;
            _discountGenerator = discountGenerator;
        }

        public Courier GetCost(Courier courier)
        {
            decimal totalCost = 0;

            if (courier == null || !courier.Parcels.Any())
            {
                return null;
            }

            BuildIndividualCost(courier);

            var orderedList = courier.Parcels.OrderBy(c => c.Cost).ToList();

            foreach (var parcel in orderedList)
            {
                totalCost += parcel.Cost;
            }

            // Apply Discount
            courier.TotalDiscountApplied = _discountGenerator.GetTotalDiscount(orderedList);

            totalCost -= courier.TotalDiscountApplied;

            courier.TotalCost = !courier.IsFastTrackEnabled ? totalCost : CalculateFastTrack(courier, totalCost);

            return courier;
        }

        private void BuildIndividualCost(Courier courier)
        {
            foreach (var parcel in courier.Parcels)
            {
                decimal weightCost;
                if (parcel.Type == ParcelType.Heavy)
                {
                    weightCost = parcel.Weight <= 50 ? 50 : 50 + _weightGenerator.CalculateWeightCost(parcel);
                    parcel.Cost = weightCost;
                }
                else
                {
                    var dimensionCost = _dimensionCostGenerator.GetDimensionCost(parcel.Dimension);
                    parcel.Type = dimensionCost.ParcelType;
                    weightCost = _weightGenerator.CalculateWeightCost(parcel);
                    parcel.Cost = dimensionCost.Cost + weightCost;
                }
            }
        }

        private decimal CalculateFastTrack(Courier courier, decimal totalCost)
        {
            var fastTrack = new FastTrack.FastTrack
            {
                Enabled = courier.IsFastTrackEnabled,
                NormalDeliveryCost = totalCost
            };

            totalCost = _fastTrackCostGenerator.CalculateCost(fastTrack);
            return totalCost;
        }
    }
}
