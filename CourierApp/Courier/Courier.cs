﻿using System.Collections.Generic;

namespace CourierApp.Courier
{
    public class Courier
    {
        public List<Parcel.Parcel> Parcels { get; set; }

        public bool IsFastTrackEnabled { get; set; }

        public decimal TotalDiscountApplied { get; set; }

        public decimal TotalCost { get; set; }
    }
}
