﻿namespace CourierApp.Courier
{
    public interface ICourierCostGenerator
    {
        Courier GetCost(Courier courier);
    }
}
