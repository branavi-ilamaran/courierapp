﻿using System;
using CourierApp.Parcel;

namespace CourierApp.Weight
{
    public class WeightGenerator : IWeightGenerator
    {
        public decimal CalculateWeightCost(Parcel.Parcel parcel)
        {
            double extraWeight;
            switch (parcel.Type)
            {
                case ParcelType.Small when parcel.Weight > 1:
                    extraWeight = parcel.Weight - 1;
                    break;
                case ParcelType.Medium when parcel.Weight > 3:
                    extraWeight = parcel.Weight - 3;
                    break;
                case ParcelType.Large when parcel.Weight > 6:
                    extraWeight = parcel.Weight - 6;
                    break;
                case ParcelType.ExtraLarge when parcel.Weight > 10:
                    extraWeight = parcel.Weight - 10;
                    break;
                case ParcelType.Heavy when parcel.Weight > 50:
                    extraWeight = parcel.Weight - 50;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (parcel.Type == ParcelType.Heavy)
            {
                return (decimal)(extraWeight * (double)1.0m);
            }

            return (decimal)(extraWeight * (double)2.0m);
        }
    }
}
