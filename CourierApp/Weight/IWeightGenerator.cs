﻿namespace CourierApp.Weight
{
    public interface IWeightGenerator
    {
        decimal CalculateWeightCost(Parcel.Parcel parcel);
    }
}
