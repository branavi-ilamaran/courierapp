﻿using System.Collections.Generic;
using Xunit;
using CourierApp.Dimension;

namespace CourierApp.Test
{
    public class DimensionCostGeneratorTest
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void Should_Return_Cost(double dimension, decimal cost)
        {
            // Arrange
            var dimensionCostGenerator = new DimensionCostGenerator();

            // Act
            var dimensionCost = dimensionCostGenerator.GetDimensionCost(dimension);

            // Assert
            Assert.NotNull(dimensionCost);
            Assert.Equal(cost, dimensionCost.Cost);
        }

        public static IEnumerable<object[]> TestData => new List<object[]>
        {
            new object[] {9, 3},
            new object[] {10, 8},
            new object[] {1000, 25},
        };

    }
}
