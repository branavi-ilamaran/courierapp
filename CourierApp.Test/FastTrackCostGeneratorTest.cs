﻿using CourierApp.FastTrack;
using Xunit;

namespace CourierApp.Test
{
    public class FastTrackCostGeneratorTest
    {
        [Fact]
        public void Should_Return_0_When_FastTrack_Is_Disabled()
        {
            // Arrange
            var fastTrack = new FastTrack.FastTrack
            {
                Enabled = false
            };

            // Act
            var cost = GenerateCalculateCost(fastTrack);

            // Assert
            Assert.Equal(0, cost);
        }

        [Fact]
        public void Should_Return_Cost_When_FastTrack_Is_Enabled()
        {
            // Arrange
            var fastTrack = new FastTrack.FastTrack
            {
                Enabled = true,
                NormalDeliveryCost = 9.7m,
            };

            // Act
            var cost = GenerateCalculateCost(fastTrack);

            // Assert
            Assert.Equal(19.4m, cost);
        }

        [Fact]
        public void Should_Return_Cost_When_AdditionalCost_Included()
        {
            // Arrange
            var fastTrack = new FastTrack.FastTrack
            {
                Enabled = true,
                NormalDeliveryCost = 9.7m,
                AdditionalCost = 10
            };

            // Act
            var cost = GenerateCalculateCost(fastTrack);

            // Assert
            Assert.Equal(29.4m, cost);
        }

        private static decimal GenerateCalculateCost(FastTrack.FastTrack fastTrack)
        {
            var fastTrackGenerator = new FastTrackCostGenerator();
            return fastTrackGenerator.CalculateCost(fastTrack);
        }
    }
}
