﻿using System.Collections.Generic;
using CourierApp.Courier;
using CourierApp.Dimension;
using CourierApp.Discount;
using CourierApp.FastTrack;
using CourierApp.Parcel;
using CourierApp.Weight;
using NSubstitute;
using Xunit;

namespace CourierApp.Test
{
    public class CourierCostGeneratorTest
    {
        private readonly CourierCostGenerator _courierCostGenerator;
        private readonly IFastTrackCostGenerator _fastTrackCostGenerator;
        private readonly IDimensionCostGenerator _dimensionCostGenerator;
        private readonly IWeightGenerator _weightGenerator;
        private readonly IDiscountGenerator _discountGenerator;

        public CourierCostGeneratorTest()
        {
            _fastTrackCostGenerator = Substitute.For<IFastTrackCostGenerator>();
            _dimensionCostGenerator = Substitute.For<IDimensionCostGenerator>();
            _weightGenerator = Substitute.For<IWeightGenerator>();
            _discountGenerator = Substitute.For<IDiscountGenerator>();
            _courierCostGenerator = new CourierCostGenerator(_dimensionCostGenerator, 
                _fastTrackCostGenerator, _weightGenerator, _discountGenerator);
        }

        [Fact]
        public void Should_Return_Cost_For_NormalDelivery()
        {
            // Arrange
            var parcels = new List<Parcel.Parcel>
            {
                new Parcel.Parcel
                {
                    Dimension = 8,
                    Weight = 2
                }
            };
            var courier = new Courier.Courier
            {
                Parcels = parcels
            };
            _dimensionCostGenerator.GetDimensionCost(Arg.Any<double>()).Returns(new DimensionCost { Cost = 3.0m });
            _weightGenerator.CalculateWeightCost(Arg.Any<Parcel.Parcel>()).Returns(2.0m);

            // Act
            var expectedCost = _courierCostGenerator.GetCost(courier);

            // Assert
            Assert.NotNull(expectedCost);
            Assert.Equal(5.0m, expectedCost.TotalCost);
        }

        [Fact]
        public void Should_Return_Cost_For_FastDelivery()
        {
            // Arrange
            var parcels = new List<Parcel.Parcel>
            {
                new Parcel.Parcel
                {
                    Dimension = 8,
                    Weight = 2
                }
            };
            var courier = new Courier.Courier
            {
                Parcels = parcels,
                IsFastTrackEnabled = true
            };
            _dimensionCostGenerator.GetDimensionCost(Arg.Any<double>()).Returns(new DimensionCost { Cost = 3.0m });
            _fastTrackCostGenerator.CalculateCost(Arg.Any<FastTrack.FastTrack>()).Returns(6.0m);
            _weightGenerator.CalculateWeightCost(Arg.Any<Parcel.Parcel>()).Returns(2.0m);


            // Act
            var expectedCourier = _courierCostGenerator.GetCost(courier);

            // Assert
            Assert.NotNull(expectedCourier);
            Assert.Equal(6.0m, expectedCourier.TotalCost);
        }

        [Fact]
        public void Should_Return_Cost_For_HeavyParcelFor50()
        {
            // Arrange
            var parcels = new List<Parcel.Parcel>
            {
                new Parcel.Parcel
                {
                    Dimension = 8,
                    Weight = 50,
                    Type = ParcelType.Heavy
                }
            };
            var courier = new Courier.Courier
            {
                Parcels = parcels
            };
            _weightGenerator.CalculateWeightCost(Arg.Any<Parcel.Parcel>()).Returns(0.0m);

            // Act
            var expectedCourier = _courierCostGenerator.GetCost(courier);

            // Assert
            Assert.NotNull(expectedCourier);
            Assert.Equal(50.0m, expectedCourier.TotalCost);
        }

        [Fact]
        public void Should_Return_Cost_For_HeavyParcelFor60()
        {
            // Arrange
            var parcels = new List<Parcel.Parcel>
            {
                new Parcel.Parcel
                {
                    Dimension = 8,
                    Weight = 60,
                    Type = ParcelType.Heavy
                }
            };
            var courier = new Courier.Courier
            {
                Parcels = parcels
            };
            _weightGenerator.CalculateWeightCost(Arg.Any<Parcel.Parcel>()).Returns(10.0m);

            // Act
            var expectedCourier = _courierCostGenerator.GetCost(courier);

            // Assert
            Assert.NotNull(expectedCourier);
            Assert.Equal(60.0m, expectedCourier.TotalCost);
        }

        [Fact]
        public void Should_Return_DiscountedCost()
        {
            // Arrange
            var courier = new Courier.Courier
            {
                Parcels = TestData.TestParcels
            };

            _dimensionCostGenerator.GetDimensionCost(Arg.Any<double>()).Returns(new DimensionCost { Cost = 3.0m });
            _weightGenerator.CalculateWeightCost(Arg.Any<Parcel.Parcel>()).Returns(2.0m);
            _discountGenerator.GetTotalDiscount(Arg.Any<List<Parcel.Parcel>>()).Returns(1);

            // Act
            var expectedCourier = _courierCostGenerator.GetCost(courier);

            // Assert
            Assert.NotNull(expectedCourier);
            Assert.Equal(24, expectedCourier.TotalCost);
            Assert.Equal(1, expectedCourier.TotalDiscountApplied);
        }
    }
}
