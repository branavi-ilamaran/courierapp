﻿using System.Collections.Generic;
using CourierApp.Parcel;
using CourierApp.Weight;
using Xunit;

namespace CourierApp.Test
{
    public class WeightCostGeneratorTest
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void Should_Return_Weight_Cost_For_Parcel_Type(Parcel.Parcel parcel, decimal expectedWeightCost)
        {
            // Act
            var weightCostGenerator = new WeightGenerator();
            var weightCost = weightCostGenerator.CalculateWeightCost(parcel);

            // Assert
            Assert.Equal(expectedWeightCost, weightCost);
        }

        public static IEnumerable<object[]> TestData => new List<object[]>
        {
            new object[] {new Parcel.Parcel{ Type = ParcelType.Small, Weight = 2 }, 2},
            new object[] {new Parcel.Parcel{ Type = ParcelType.Medium, Weight =  6}, 6},
            new object[] {new Parcel.Parcel{ Type = ParcelType.Large, Weight = 9}, 6},
            new object[] {new Parcel.Parcel { Type = ParcelType.Heavy, Weight = 55}, 5}
        };
    }
}
