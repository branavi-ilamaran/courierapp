﻿using CourierApp.Discount;
using Xunit;

namespace CourierApp.Test
{
    public class DiscountGeneratorTest
    {
        [Fact]
        public void Should_Return_Total_Discount()
        {
            // Act
            var discountGenerator = new DiscountGenerator();
            var discountedAmount = discountGenerator.GetTotalDiscount(TestData.TestParcels);

            // Assert
            Assert.Equal(35, discountedAmount);
        }
    }
}
