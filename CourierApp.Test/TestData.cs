﻿using System.Collections.Generic;
using CourierApp.Parcel;

namespace CourierApp.Test
{
    public static class TestData
    {
        public static List<Parcel.Parcel> TestParcels = new List<Parcel.Parcel>
        {
            new Parcel.Parcel
            {
                Dimension = 8,
                Weight = 2,
                Type = ParcelType.Small,
                Cost = 2,
            },
            new Parcel.Parcel
            {
                Dimension = 30,
                Weight = 3,
                Type = ParcelType.Medium,
                Cost = 5,
            },
            new Parcel.Parcel
            {
                Dimension = 35,
                Weight = 3,
                Type = ParcelType.Medium,
                Cost = 10,
            },
            new Parcel.Parcel
            {
                Dimension = 44,
                Weight = 3,
                Type = ParcelType.Medium,
                Cost = 15,
            },
            new Parcel.Parcel
            {
                Dimension = 70,
                Weight = 2,
                Type = ParcelType.Large,
                Cost = 20,
            }
        };
    }
}
